<?php
	include_once('connexion_base.php');
	

	$array = [10, 8, 9, 11, 63, 3];

	/**
	 * Returns ordered given $array ascending or descending
	 * 
	 * @param $array
	 * @param string $ascOrDesc
	 * @return mixed
	 */
	function order($array, $ascOrDesc) {
		if(strcmp($ascOrDesc,'asc') == 0){
			rsort($array);
		}else{
			if(strcmp($ascOrDesc,'desc') == 0){
				sort($array);
			}
		}
	    return $array;
	}

	function afficheTableau($array){
		foreach ($array as $key => $value) {
			echo "{$key} => {$value} <br>";
		}
	}
	
	//affichage 
	echo "Tableau ascendant: <br>";
	afficheTableau(order($array, 'asc'));
	echo "<br>";
	echo "Tableau descendant: <br>";
	afficheTableau(order($array, 'desc'));

	//var_dump(order($array, 'desc'));

	//var_dump(order($array, 'asc'));

	/**
	 * Returns odd or even number from given $array
	 *
	 * @param $array
	 * @param string $oddOrEven
	 * @return mixed
	 */
	function oddOrEven($array, $oddOrEven) {
		$tab = array();
		$j=0;
		if(strcmp($oddOrEven,'even') == 0){
			for ($i=0; $i <count($array) ; $i++) { 
				if($array[$i] % 2 == 0){
					$tab[$j] =$array[$i];
					$j++;
				}
			}
		}else{
			if(strcmp($oddOrEven,'odd') == 0){
				for ($i=0; $i <count($array) ; $i++) { 
					if($array[$i] % 2 != 0){
						$tab[$j] =$array[$i];
						$j++;
					}
				}
			}
		}
		return $tab;
	}	

	function requette(){
		$db=pdo_connexion();
		$req =$db -> prepare("SELECT a.id,a.auteur,COUNT(*) AS livre
									FROM auteur a INNER JOIN livre l
									ON a.id_livre=l.id
									GROUP BY a.auteur;");
		$req -> execute(array());
		return $req;
	}

	function afficheRequette($req){
		while ($data = $req->fetch()) {
	      echo $data['id'].' - '.$data['auteur'].' - '.$data['livre'].'<br>';
	    }
	}

	
	
?>
	